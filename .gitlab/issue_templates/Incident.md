<!---
Create a dedicated Zoom call for the incident
-->
/zoom

<!---
By default, we use the #production Slack channel rather than a dedicated per-incident channel
-->
/slack #production

<!---
Notify the standard on-call pager 
-->
/pagerduty on-call-service

<!---
Label the issue as an incident
-->
/label ~incident

---

<!--- 
Incident specific content is automatically appended to the template, and will go here
-->